﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Beletzkaya
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Hash();

        }

        public static string Hash()
        {
            var strAlgName = HashAlgorithmNames.Md5;
            var objAlgProv = HashAlgorithmProvider.OpenAlgorithm(strAlgName);
            var objHash = objAlgProv.CreateHash();
            var rnd = new Random();     
            var strMsg1 = DateTime.Now.Millisecond* rnd.Next();
            var buffMsg1 = CryptographicBuffer.ConvertStringToBinary(strMsg1.ToString(), BinaryStringEncoding.Utf16BE);
                objHash.Append(buffMsg1);
            var buffHash1 = objHash.GetValueAndReset();
            var strHash1 = CryptographicBuffer.EncodeToBase64String(buffHash1);
            return strHash1;
        }

        public static string UnixTimeStampToDateTime()
        {
            var unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp.ToString();
        }
        public string getAllMAterials()
        {
            var req = new Request()
            {
                consumer_key = "433131",

                consumer_secret = "d7yPwmmHeFpeFTNBTYlWiWUWnQ9q9Q",

                oauth_token = "VKeb422vrCZIPZmtFyHju7bN7TqgjyM4fWpGe7O7",

                oauth_token_secret = "H.60ktxzoHA4yOp9XiizWAj1ifZuol1EN6lCOWOL",

                main_url = "http://beletzkaya.ucoz.ru/uapi"

            };


        }
    }

    class Request
    {
        public string oauth_nonce { get { return  MainPage.Hash(); } set {} }
        public string timestamp { get { return MainPage.UnixTimeStampToDateTime(); } set { } }
        public string sig_method { get { return "HMAC-SHA1"; } set { } }
        public string oauth_version { get { return "1.0"; } set {} }
        public string consumer_key { get; set; }
        public string consumer_secret { get; set; }
        public string oauth_token { get; set; }
        public string oauth_token_secret { get; set; }
        public string main_url { get; set; }


    }
 
}
